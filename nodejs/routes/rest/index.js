// centralize all exports in this index file and configurate their routes
var express = require('express');
var router = express.Router();

router.use('/categories', require('./categoriesRouteFinal'));
router.use('/categories1', require('./categoriesRoute1'));
router.use('/categories2', require('./categoriesRoute2'));
router.use('/events', require('./eventsRoute'));

module.exports = router;
