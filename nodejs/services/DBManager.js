// module dependencies
let MongoClient = require('mongodb').MongoClient;
// saves the connection to different MongoDBs
let connections = new Map();

// returns object with a function to get a db connection
module.exports =  {
  getConnection: (dbURL, callback) => {
    // if string is empty we can't connect to db
    if(!dbURL || dbURL.length === 0){
      console.log("error: empty string");
      return;
    }

    // try to get the connection from our map
    let connectionPool = connections.get(dbURL);

    // return connection if we had one in our map, else create a new connectionPool, save it in our map and return it
    if(connectionPool) {
      callback(connectionPool);
    } else {
      MongoClient.connect(dbURL, (err, connectionPool) => {
        if(err) throw err;
        connections.set(dbURL, connectionPool);
        callback(connectionPool);
      });
    }
  }
};
