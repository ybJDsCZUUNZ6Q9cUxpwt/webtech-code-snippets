const ObjectId = require('mongodb').ObjectId;
let DBManager = require('./DBManager');
let Event = require('../model/event');
let db, eventCollection;

// export function which creates our eventService object
module.exports = function eventsService(dbURL) {
   DBManager.getConnection(dbURL, (database) => {
    db = database;
    eventCollection = db.collection('events');
  });
  // saves an event in the db
  eventsService.create =  (event, callback) => {
    eventCollection.insert(event, (err, result) => {
      if (err) throw err;
      callback(event._id);
    })
  }
  // gets all event
  eventsService.listAll = (callback) => {
    eventsService.listAllByQuery({}, callback);
  }
  // gets all event found by query, mostly used to get all event from one category
  eventsService.listAllByQuery =  (query, callback) => {
    eventCollection.find(query).toArray( (err, array) => {
      if (err) throw err;
      callback(array);
    })
  }
  // gets an event with the specified id from db
  eventsService.findById = (id, callback) => {
    eventCollection.findOne({"_id": new ObjectId(id)}, (err, item) => {
      callback(item);
    });
  }
  return eventsService;
}
