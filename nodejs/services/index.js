let config = require('../config.json');

// centralize all exports in this index file and create the objects with the right config
module.exports = {
  DBManager: require('./DBManager'),
  events: require('./eventsService')(config.dbURL),
  categories: require('./categoriesService')(config.dbURL),
}
