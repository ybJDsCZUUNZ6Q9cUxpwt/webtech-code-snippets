(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/add-flatmate/add-flatmate.component.css":
/*!*********************************************************!*\
  !*** ./src/app/add-flatmate/add-flatmate.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkZC1mbGF0bWF0ZS9hZGQtZmxhdG1hdGUuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/add-flatmate/add-flatmate.component.html":
/*!**********************************************************!*\
  !*** ./src/app/add-flatmate/add-flatmate.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [hidden]=\"!userCreated\" class=\"alert alert-success\">Mitbewohner hinzugefügt</div>\r\n\r\n<form name=\"newFlatmateForm\" class=\"form-horizontal\" novalidate>\r\n  <fieldset>\r\n\r\n    <!-- Form Name -->\r\n    <legend>Neuer Mitbewohner</legend>\r\n\r\n    <!-- Text input-->\r\n    <div class=\"form-group\">\r\n      <label class=\"col-md-2 control-label\" for=\"name\">Name</label>\r\n      <div class=\"col-md-10\">\r\n        <input [(ngModel)]=\"newFlatmate.name\" id=\"name\" name=\"name\" type=\"text\" placeholder=\"Name\" class=\"form-control input-md\" required #name=\"ngModel\"> \r\n      </div>\r\n    </div>\r\n\r\n    <!-- Button -->\r\n    <div class=\"form-group\">\r\n      <div class=\"col-md-offset-6 col-md-6\">\r\n        <input class=\"btn btn-primary btn-block\" type=\"submit\" value=\"Mitbewohner hinzufügen\" (click)=\"addFlatmate()\" [disabled]=\"name.invalid\"> \r\n      </div>\r\n    </div>\r\n  </fieldset>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/add-flatmate/add-flatmate.component.ts":
/*!********************************************************!*\
  !*** ./src/app/add-flatmate/add-flatmate.component.ts ***!
  \********************************************************/
/*! exports provided: AddFlatmateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddFlatmateComponent", function() { return AddFlatmateComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_communicator_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../http-communicator.service */ "./src/app/http-communicator.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddFlatmateComponent = /** @class */ (function () {
    function AddFlatmateComponent(http) {
        this.http = http;
        this.userCreated = false;
        this.newFlatmate = {
            id: -1,
            name: ''
        };
    }
    AddFlatmateComponent.prototype.addFlatmate = function () {
        var _this = this;
        this.http.addFlatmate(this.newFlatmate).subscribe(function (resp) {
            if (resp.status === 200) {
                _this.userCreated = true;
            }
        });
    };
    AddFlatmateComponent.prototype.ngOnInit = function () {
    };
    AddFlatmateComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-flatmate',
            template: __webpack_require__(/*! ./add-flatmate.component.html */ "./src/app/add-flatmate/add-flatmate.component.html"),
            styles: [__webpack_require__(/*! ./add-flatmate.component.css */ "./src/app/add-flatmate/add-flatmate.component.css")]
        }),
        __metadata("design:paramtypes", [_http_communicator_service__WEBPACK_IMPORTED_MODULE_1__["HttpCommunicatorService"]])
    ], AddFlatmateComponent);
    return AddFlatmateComponent;
}());



/***/ }),

/***/ "./src/app/add-purchase/add-purchase.component.css":
/*!*********************************************************!*\
  !*** ./src/app/add-purchase/add-purchase.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkZC1wdXJjaGFzZS9hZGQtcHVyY2hhc2UuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/add-purchase/add-purchase.component.html":
/*!**********************************************************!*\
  !*** ./src/app/add-purchase/add-purchase.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [hidden]=\"!purchaseCreated\" class=\"alert alert-success\">Kauf hinzugefügt</div>\n\n<form class=\"form-horizontal\">\n  <fieldset>\n    <!-- Form Name -->\n    <legend>Neuer Kauf</legend>\n    <!-- Text input-->\n    <div class=\"form-group\">\n      <label class=\"col-md-2 control-label\" for=\"title\">Titel</label>\n      <div class=\"col-md-10\">\n        <input id=\"title\" name=\"title\" type=\"text\" placeholder=\"Titel\" class=\"form-control input-md\" required #title=\"ngModel\" [(ngModel)]=\"newPurchase.title\">\n      </div>\n    </div>\n    <!-- Textarea -->\n    <div class=\"form-group\">\n      <label class=\"col-md-2 control-label\" for=\"description\">Beschreibung</label>\n      <div class=\"col-md-10\">\n        <textarea class=\"form-control\" id=\"description\" name=\"description\" placeholder=\"2kg Kartoffeln, 2l Milch, Joghurt\" [(ngModel)]=\"newPurchase.description\"></textarea>\n      </div>\n    </div>\n    <!-- Appended Input-->\n    <div class=\"form-group\">\n      <label class=\"col-md-2 control-label\" for=\"cost\">Preis</label>\n      <div class=\"col-md-10\">\n        <div class=\"input-group\">\n          <input id=\"cost\" name=\"cost\" class=\"form-control\" placeholder=\"0.00\" type=\"number\" step=\"0.01\" required #cost=\"ngModel\" [(ngModel)]=\"purchaseCost\">\n          <span class=\"input-group-addon\">€</span>\n        </div>\n      </div>\n    </div>\n    <!-- Text input-->\n    <div class=\"form-group\">\n      <label class=\"col-md-2 control-label\" for=\"date\">Datum</label>\n      <div class=\"col-md-10\">\n        <input id=\"date\" name=\"date\" type=\"datetime-local\" class=\"form-control input-md\" required=\"\" [(ngModel)]=\"newPurchase.date\" placeholder=\"2018-01-01\">\n      </div>\n    </div>\n    <!-- Select Basic -->\n    <div class=\"form-group\">\n      <label class=\"col-md-2 control-label\" for=\"boughtBy\">Käufer</label>\n      <div class=\"col-md-10\">\n        <select id=\"boughtBy\" name=\"boughtBy\" [(ngModel)]=\"newPurchase.boughtBy\" class=\"form-control\">\n          <option *ngFor=\"let f of flatmates\">{{f.name}}</option>\n          <!--\n        <?php foreach($variables['flatMates'] as $flatMate) { ?>\n            <option value=\"<?= $flatMate->getId(); ?>\"><?= $flatMate->getName(); ?></option>\n        <?php } ?>-->\n        </select>\n      </div>\n    </div>\n    <!-- Select Multiple -->\n    <div class=\"form-group\">\n      <label class=\"col-md-2 control-label\" for=\"boughtFor\">Gekauft Für</label>\n      <div class=\"col-md-10\">\n        <select id=\"boughtFor\" name=\"boughtFor[]\" class=\"form-control\" [(ngModel)]=\"newPurchase.boughtFor\" multiple=\"multiple\" required #boughtFor=\"ngModel\">\n          <option *ngFor=\"let f of flatmates\">{{f.name}}</option>\n          <!--\n        <?php foreach($variables['flatMates'] as $flatMate) { ?>\n            <option value=\"<?= $flatMate->getId(); ?>\"><?= $flatMate->getName(); ?></option>\n        <?php } ?>-->\n        </select>\n      </div>\n    </div>\n    <!-- Button -->\n    <div class=\"form-group\">\n      <div class=\"col-md-offset-6 col-md-6\">\n        <input class=\"btn btn-primary btn-block\" type=\"submit\" (click)=\"addPurchase()\" value=\"Kauf Speichern\" [disabled]=\"title.invalid || cost.invalid || boughtFor.invalid\">\n      </div>\n    </div>\n  </fieldset>\n</form>\n"

/***/ }),

/***/ "./src/app/add-purchase/add-purchase.component.ts":
/*!********************************************************!*\
  !*** ./src/app/add-purchase/add-purchase.component.ts ***!
  \********************************************************/
/*! exports provided: AddPurchaseComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPurchaseComponent", function() { return AddPurchaseComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_communicator_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../http-communicator.service */ "./src/app/http-communicator.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddPurchaseComponent = /** @class */ (function () {
    function AddPurchaseComponent(http) {
        var _this = this;
        this.http = http;
        this.flatmates = [];
        this.newPurchase = {
            id: -1,
            title: '',
            description: '',
            cost: undefined,
            date: '',
            boughtBy: '',
            boughtFor: []
        };
        this.purchaseCreated = false;
        //this.http.getAllFlatmates().subscribe(data => this.flatmates = data);
        this.http.getAllFlatmates().subscribe(function (resp) {
            if (resp.status === 200) {
                _this.flatmates = resp.body;
                _this.newPurchase.boughtBy = _this.flatmates[0].name;
            }
        });
    }
    AddPurchaseComponent.prototype.addPurchase = function () {
        var _this = this;
        this.newPurchase.cost = this.purchaseCost * 100;
        console.log(this.newPurchase);
        this.http.addPurchase(this.newPurchase).subscribe(function (resp) {
            if (resp.status === 200) {
                _this.purchaseCreated = true;
            }
        });
    };
    AddPurchaseComponent.prototype.ngOnInit = function () {
    };
    AddPurchaseComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-purchase',
            template: __webpack_require__(/*! ./add-purchase.component.html */ "./src/app/add-purchase/add-purchase.component.html"),
            styles: [__webpack_require__(/*! ./add-purchase.component.css */ "./src/app/add-purchase/add-purchase.component.css")]
        }),
        __metadata("design:paramtypes", [_http_communicator_service__WEBPACK_IMPORTED_MODULE_1__["HttpCommunicatorService"]])
    ], AddPurchaseComponent);
    return AddPurchaseComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_flatmate_add_flatmate_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add-flatmate/add-flatmate.component */ "./src/app/add-flatmate/add-flatmate.component.ts");
/* harmony import */ var _add_purchase_add_purchase_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-purchase/add-purchase.component */ "./src/app/add-purchase/add-purchase.component.ts");
/* harmony import */ var _show_purchases_show_purchases_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./show-purchases/show-purchases.component */ "./src/app/show-purchases/show-purchases.component.ts");
/* harmony import */ var _balances_balances_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./balances/balances.component */ "./src/app/balances/balances.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: '', component: _add_flatmate_add_flatmate_component__WEBPACK_IMPORTED_MODULE_2__["AddFlatmateComponent"] },
    { path: 'addPurchase', component: _add_purchase_add_purchase_component__WEBPACK_IMPORTED_MODULE_3__["AddPurchaseComponent"] },
    { path: 'showPurchases', component: _show_purchases_show_purchases_component__WEBPACK_IMPORTED_MODULE_4__["ShowPurchasesComponent"] },
    { path: 'balances', component: _balances_balances_component__WEBPACK_IMPORTED_MODULE_5__["BalancesComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <nav class=\"navbar navbar-default\">\r\n    <div class=\"container-fluid\">\r\n      <div class=\"navbar-header\">\r\n        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\r\n          <span class=\"sr-only\">Toggle navigation</span>\r\n          <span class=\"icon-bar\"></span>\r\n          <span class=\"icon-bar\"></span>\r\n          <span class=\"icon-bar\"></span>\r\n        </button>\r\n        <a class=\"navbar-brand\" href=\"\">WG Finanzen</a>\r\n      </div>\r\n      <div id=\"navbar\" class=\"navbar-collapse collapse\">\r\n        <ul class=\"nav navbar-nav\">\r\n          <li class=\"{{isActive('/')}}\"><a routerLink=\"/\">Neuer Mitbewohner</a></li>\r\n          <li class=\"{{isActive('/addPurchase')}}\"><a routerLink=\"/addPurchase\">Neuer Kauf</a></li>\r\n          <li class=\"{{isActive('/showPurchases')}}\"><a routerLink=\"/showPurchases\">Käufe</a></li>\r\n          <li class=\"{{isActive('/balances')}}\"><a routerLink=\"/balances\">Bilanzen</a></li>\r\n        </ul>\r\n      </div><!--/.nav-collapse -->\r\n    </div><!--/.container-fluid -->\r\n  </nav>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = /** @class */ (function () {
    function AppComponent(router, titleService) {
        this.router = router;
        this.titleService = titleService;
    }
    AppComponent.prototype.setPageTitle = function () {
        var currentTitle = 'WG Finanzen';
        switch (this.router.url) {
            case '/':
                currentTitle = currentTitle + ' - Neuer Mitbewohner';
                break;
            case '/addPurchase':
                currentTitle = currentTitle + ' - Neuer Kauf';
                break;
            case '/showPurchases':
                currentTitle = currentTitle + ' - Käufe';
                break;
            case '/balances':
                currentTitle = currentTitle + ' - Bilanzen';
                break;
        }
        this.titleService.setTitle(currentTitle);
    };
    AppComponent.prototype.isActive = function (path) {
        if (path === '/') {
            this.setPageTitle();
        }
        return (path === this.router.url) ? 'active' : '';
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _enlarge_directive__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./enlarge.directive */ "./src/app/enlarge.directive.ts");
/* harmony import */ var _temp_info_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./temp-info.directive */ "./src/app/temp-info.directive.ts");
/* harmony import */ var _add_flatmate_add_flatmate_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-flatmate/add-flatmate.component */ "./src/app/add-flatmate/add-flatmate.component.ts");
/* harmony import */ var _add_purchase_add_purchase_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./add-purchase/add-purchase.component */ "./src/app/add-purchase/add-purchase.component.ts");
/* harmony import */ var _balances_balances_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./balances/balances.component */ "./src/app/balances/balances.component.ts");
/* harmony import */ var _show_purchases_show_purchases_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./show-purchases/show-purchases.component */ "./src/app/show-purchases/show-purchases.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _enlarge_directive__WEBPACK_IMPORTED_MODULE_6__["EnlargeDirective"],
                _temp_info_directive__WEBPACK_IMPORTED_MODULE_7__["TempInfoDirective"],
                _add_flatmate_add_flatmate_component__WEBPACK_IMPORTED_MODULE_8__["AddFlatmateComponent"],
                _add_purchase_add_purchase_component__WEBPACK_IMPORTED_MODULE_9__["AddPurchaseComponent"],
                _balances_balances_component__WEBPACK_IMPORTED_MODULE_10__["BalancesComponent"],
                _show_purchases_show_purchases_component__WEBPACK_IMPORTED_MODULE_11__["ShowPurchasesComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/balances/balances.component.css":
/*!*************************************************!*\
  !*** ./src/app/balances/balances.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JhbGFuY2VzL2JhbGFuY2VzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/balances/balances.component.html":
/*!**************************************************!*\
  !*** ./src/app/balances/balances.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table\">\n  <thead>\n  <tr>\n      <th>Mitbewohner</th>\n      <th>Schuldet</th>\n      <th>Betrag</th>\n  </tr>\n  </thead>\n  <tbody>\n      <tr *ngFor=\"let r of rows\">\n          <td>{{r.name}}</td>\n          <td>{{r.to}}</td>\n          <td>{{r.amount/100}}&euro;</td>\n      </tr>\n  </tbody>\n</table>\n"

/***/ }),

/***/ "./src/app/balances/balances.component.ts":
/*!************************************************!*\
  !*** ./src/app/balances/balances.component.ts ***!
  \************************************************/
/*! exports provided: BalancesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BalancesComponent", function() { return BalancesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_communicator_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../http-communicator.service */ "./src/app/http-communicator.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BalancesComponent = /** @class */ (function () {
    function BalancesComponent(http) {
        var _this = this;
        this.http = http;
        this.flatmates = [];
        this.purchases = [];
        this.rows = [];
        this.whoToWho = [];
        this.http.getAllFlatmates().subscribe(function (resp) {
            if (resp.status === 200) {
                _this.flatmates = resp.body;
                _this.setupWhoToWho();
                _this.getAllPurchases();
            }
        });
    }
    BalancesComponent.prototype.getAllPurchases = function () {
        var _this = this;
        this.http.getAllPurchases().subscribe(function (resp) {
            if (resp.status === 200) {
                _this.purchases = resp.body;
                _this.computeBalances();
            }
        });
    };
    BalancesComponent.prototype.getIDByName = function (name) {
        var id = -1;
        this.flatmates.forEach(function (flatmate) {
            if (flatmate.name === name) {
                id = flatmate.id;
            }
        });
        return id;
    };
    BalancesComponent.prototype.setupWhoToWho = function () {
        var _this = this;
        this.flatmates.forEach(function (flatmate) {
            _this.whoToWho[flatmate.name] = {};
        });
        var keys = Object.keys(this.whoToWho);
        var i; //*
        for (i = 0; i < keys.length; i = i + 1) {
            this.flatmates.forEach(function (flatmate) {
                if (keys[i] != flatmate.name) {
                    _this.whoToWho[keys[i]][flatmate.name] = 0;
                }
            });
        } //*/
    };
    BalancesComponent.prototype.computeBalances = function () {
        var _this = this;
        this.purchases.forEach(function (purchase) {
            var dif = purchase.cost / purchase.boughtFor.length;
            purchase.boughtFor.forEach(function (flatmate) {
                if (flatmate != purchase.boughtBy) {
                    _this.whoToWho[flatmate][purchase.boughtBy] = _this.whoToWho[flatmate][purchase.boughtBy] + dif;
                }
            });
        });
        this.prepareBalances();
    };
    BalancesComponent.prototype.prepareBalances = function () {
        var mainKeys = Object.keys(this.whoToWho);
        var i, j;
        for (i = 0; i < mainKeys.length; i = i + 1) {
            var subkeys = Object.keys(this.whoToWho[mainKeys[i]]);
            for (j = 0; j < subkeys.length; j = j + 1) {
                if (this.whoToWho[mainKeys[i]][subkeys[j]] > 0) {
                    this.rows.push({
                        name: mainKeys[i],
                        to: subkeys[j],
                        amount: this.whoToWho[mainKeys[i]][subkeys[j]]
                    });
                }
            }
        }
    };
    BalancesComponent.prototype.ngOnInit = function () {
    };
    BalancesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-balances',
            template: __webpack_require__(/*! ./balances.component.html */ "./src/app/balances/balances.component.html"),
            styles: [__webpack_require__(/*! ./balances.component.css */ "./src/app/balances/balances.component.css")]
        }),
        __metadata("design:paramtypes", [_http_communicator_service__WEBPACK_IMPORTED_MODULE_1__["HttpCommunicatorService"]])
    ], BalancesComponent);
    return BalancesComponent;
}());



/***/ }),

/***/ "./src/app/enlarge.directive.ts":
/*!**************************************!*\
  !*** ./src/app/enlarge.directive.ts ***!
  \**************************************/
/*! exports provided: EnlargeDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnlargeDirective", function() { return EnlargeDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EnlargeDirective = /** @class */ (function () {
    function EnlargeDirective(element) {
        this.element = element;
    }
    EnlargeDirective.prototype.enlarge = function (value) {
        this.element.nativeElement.style.fontSize = value + '%';
    };
    EnlargeDirective.prototype.onMouseEnter = function () {
        this.enlarge(this.zoomValue);
    };
    EnlargeDirective.prototype.onMouseLeave = function () {
        this.enlarge(100);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('appEnlarge'),
        __metadata("design:type", Number)
    ], EnlargeDirective.prototype, "zoomValue", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('mouseenter'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], EnlargeDirective.prototype, "onMouseEnter", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('mouseleave'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], EnlargeDirective.prototype, "onMouseLeave", null);
    EnlargeDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appEnlarge]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], EnlargeDirective);
    return EnlargeDirective;
}());



/***/ }),

/***/ "./src/app/http-communicator.service.ts":
/*!**********************************************!*\
  !*** ./src/app/http-communicator.service.ts ***!
  \**********************************************/
/*! exports provided: HttpCommunicatorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpCommunicatorService", function() { return HttpCommunicatorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HttpCommunicatorService = /** @class */ (function () {
    function HttpCommunicatorService(http) {
        this.http = http;
        //apiUrl: string = 'http://localhost:8080/api/v1/';
        this.apiUrl = '/api/v1/';
        this.flatmateUrl = 'flatmates';
        this.purchaseUrl = 'purchases';
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
    }
    HttpCommunicatorService.prototype.getAllFlatmates = function () {
        return this.http.get(this.apiUrl + this.flatmateUrl, { observe: 'response' });
    };
    HttpCommunicatorService.prototype.addFlatmate = function (newFlatmate) {
        return this.http.post(this.apiUrl + this.flatmateUrl, newFlatmate, { headers: this.httpOptions.headers, observe: 'response' });
    };
    HttpCommunicatorService.prototype.getAllPurchases = function () {
        return this.http.get(this.apiUrl + this.purchaseUrl, { observe: 'response' });
    };
    HttpCommunicatorService.prototype.addPurchase = function (newPurchase) {
        return this.http.post(this.apiUrl + this.purchaseUrl, newPurchase, { headers: this.httpOptions.headers, observe: 'response' });
    };
    HttpCommunicatorService.prototype.deletePurchase = function (id) {
        return this.http.delete(this.apiUrl + this.purchaseUrl + '/' + id, { observe: 'response' });
    };
    HttpCommunicatorService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], HttpCommunicatorService);
    return HttpCommunicatorService;
}());



/***/ }),

/***/ "./src/app/show-purchases/show-purchases.component.css":
/*!*************************************************************!*\
  !*** ./src/app/show-purchases/show-purchases.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Nob3ctcHVyY2hhc2VzL3Nob3ctcHVyY2hhc2VzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/show-purchases/show-purchases.component.html":
/*!**************************************************************!*\
  !*** ./src/app/show-purchases/show-purchases.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [hidden]=\"!purchaseRemoved\" class=\"alert alert-success\">Kauf gelöscht</div>\n<div [hidden]=\"!purchaseNotRemoved\" class=\"alert alert-warning\">Kauf nicht gefunden</div>\n\n<table class=\"table\">\n    <thead>\n    <tr>\n        <th>Datum</th>\n        <th>Titel</th>\n        <th>Preis</th>\n        <th>Käufer</th>\n        <th class=\"hidden-xs\">Beschreibung</th>\n        <th>Für</th>\n        <th class=\"text-center\">Löschen</th>\n    </tr>\n    </thead>\n    <tbody>\n        <tr *ngFor=\"let p of purchases\">\n            <td>{{p.date}}</td>\n            <td>{{p.title}}</td>\n            <td>{{p.cost/100}}&euro;</td>\n            <td>{{p.boughtBy}}</td>\n            <td>{{p.description}}</td>\n            <td>{{p.boughtFor}}</td>\n            <td class=\"text-center\">\n                <form method=\"post\" action=\"\">\n                    <input type=\"hidden\" name=\"id\" value=\"{{p.id}}\">\n                    <button class=\"btn btn-xs btn-default\" type=\"button\" title=\"Löschen\" (click)=\"deletePurchase(p.id)\"><i class=\"glyphicon glyphicon-remove\"></i></button>\n                </form>\n            </td>\n        </tr>\n    </tbody>\n</table>\n"

/***/ }),

/***/ "./src/app/show-purchases/show-purchases.component.ts":
/*!************************************************************!*\
  !*** ./src/app/show-purchases/show-purchases.component.ts ***!
  \************************************************************/
/*! exports provided: ShowPurchasesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowPurchasesComponent", function() { return ShowPurchasesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _http_communicator_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../http-communicator.service */ "./src/app/http-communicator.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ShowPurchasesComponent = /** @class */ (function () {
    function ShowPurchasesComponent(http) {
        this.http = http;
        this.purchaseRemoved = false;
        this.purchaseNotRemoved = false;
        this.purchases = [];
        this.getAllPurchases();
    }
    ShowPurchasesComponent.prototype.getAllPurchases = function () {
        var _this = this;
        this.http.getAllPurchases().subscribe(function (resp) {
            if (resp.status === 200) {
                _this.purchases = resp.body;
            }
        });
    };
    ShowPurchasesComponent.prototype.deletePurchase = function (id) {
        var _this = this;
        this.http.deletePurchase(id).subscribe(function (resp) {
            if (resp.status === 200) {
                _this.purchaseRemoved = true;
                _this.purchaseNotRemoved = false;
                _this.getAllPurchases();
            }
            else {
                _this.purchaseRemoved = false;
                _this.purchaseNotRemoved = true;
            }
        });
    };
    ShowPurchasesComponent.prototype.ngOnInit = function () {
    };
    ShowPurchasesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-show-purchases',
            template: __webpack_require__(/*! ./show-purchases.component.html */ "./src/app/show-purchases/show-purchases.component.html"),
            styles: [__webpack_require__(/*! ./show-purchases.component.css */ "./src/app/show-purchases/show-purchases.component.css")]
        }),
        __metadata("design:paramtypes", [_http_communicator_service__WEBPACK_IMPORTED_MODULE_1__["HttpCommunicatorService"]])
    ], ShowPurchasesComponent);
    return ShowPurchasesComponent;
}());



/***/ }),

/***/ "./src/app/temp-info.directive.ts":
/*!****************************************!*\
  !*** ./src/app/temp-info.directive.ts ***!
  \****************************************/
/*! exports provided: TempInfoDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TempInfoDirective", function() { return TempInfoDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TempInfoDirective = /** @class */ (function () {
    function TempInfoDirective(templateRef, viewContainerRef) {
        this.templateRef = templateRef;
        this.viewContainerRef = viewContainerRef;
        this.viewContainerRef.createEmbeddedView(this.templateRef);
    }
    Object.defineProperty(TempInfoDirective.prototype, "removeDelay", {
        set: function (time) {
            var _this = this;
            setTimeout(function () {
                _this.viewContainerRef.clear();
            }, time);
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('appTempInfo'),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], TempInfoDirective.prototype, "removeDelay", null);
    TempInfoDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appTempInfo]'
        }),
        __metadata("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]])
    ], TempInfoDirective);
    return TempInfoDirective;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\lufg\Documents\Angular-Schritt_fuer_Schritt\angular-client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map