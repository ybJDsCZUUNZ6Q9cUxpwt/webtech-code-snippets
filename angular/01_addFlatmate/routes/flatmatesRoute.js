var router = require('express').Router();

var Supporter = require('../modules/support.js');

var sup = new Supporter();

var flatmates = [
    {
        id: 0,
        name: 'Alice'
    },
    {
        id: 1,
        name: 'Bob'
    },
    {
        id: 2,
        name: 'Eve'
    }
];

router.get('/', function(req, res) {
    res.json(flatmates);
});

router.get('/:id', function(req, res){
    if(flatmates[req.params.id] === undefined){
        res.send('No such flatmate available');
    }
    else{
        res.send(flatmates[req.params.id]);
    }
});

router.post('/', function(req, res){
    if(sup.isUndefinedOrEmpty(req.body.name)){
        res.status(400).send('name required');
    }
    else{
        var newFlatmate = {
            id: flatmates.length,
            name: req.body.name
        };
        flatmates.push(newFlatmate);
        res.status(200).send(flatmates[flatmates.length-1]);
    }
});

module.exports = router;