import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Flatmate } from './flatmate';

@Injectable({
  providedIn: 'root'
})
export class HttpCommunicatorService {

  //apiUrl: string = 'http://localhost:8080/api/v1/';
  apiUrl: string = '/api/v1/';
  flatmateUrl: string = 'flatmates';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  public getAllFlatmates(): Observable<HttpResponse<Flatmate[]>> {
    return this.http.get<Flatmate[]>(this.apiUrl + this.flatmateUrl, { observe: 'response' });
  }
  
  public addFlatmate(newFlatmate: Flatmate): Observable<HttpResponse<Flatmate>> {
    return this.http.post<Flatmate>(this.apiUrl + this.flatmateUrl, newFlatmate, { headers: this.httpOptions.headers, observe: 'response'});
  }

}
