import { Component, OnInit } from '@angular/core';
import { Flatmate } from '../flatmate';
import { HttpCommunicatorService } from '../http-communicator.service';

@Component({
  selector: 'app-add-flatmate',
  templateUrl: './add-flatmate.component.html',
  styleUrls: ['./add-flatmate.component.css']
})
export class AddFlatmateComponent implements OnInit {

    userCreated: boolean = false;
  
    newFlatmate: Flatmate = {
      id: -1,
      name: ''
    };
    
    addFlatmate() {
      this.http.addFlatmate(this.newFlatmate).subscribe(resp => {
        if (resp.status === 200) {
          this.userCreated = true;
        }
      });
    }

    constructor( private http: HttpCommunicatorService ) {}

    ngOnInit() {
    }

}
